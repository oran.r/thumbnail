package com.cimbthai.digital.controller;

import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification;
import com.cimbthai.digital.config.Config;
import com.cimbthai.digital.service.ImageService;
import net.coobird.thumbnailator.Thumbnails;

import java.io.*;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ImageController {

    private ImageService service = new ImageService();

    public void resizeImage(S3Event s3Event) throws IOException {
        Config config = Config.getInstance();

        for (S3EventNotification.S3EventNotificationRecord eventRecord : s3Event.getRecords()) {
            String srcBucket = getBucketName(eventRecord);
            String srcKey = getKey(eventRecord);
            validateFile(srcKey);

            // Read the source image
            InputStream imageInputStream = service.getImageStream(srcBucket, srcKey);

            // Create Thumbnail
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            Thumbnails.of(imageInputStream)
                    .size(config.getMaxWidth(), config.getMaxHeight())
//                    .outputFormat("jpg")
                    .outputQuality(0.9)
                    //.watermark(Positions.BOTTOM_RIGHT, watermarkImage, 0.5f)
                    .toOutputStream(os);
            InputStream is = new ByteArrayInputStream(os.toByteArray());

            // Upload to S3 destination bucket
            String destKey = srcKey;
            service.save(is, destKey, os.size(), getContentType(srcKey));
            System.out.printf("Successfully resized %s/%s and uploaded to %s/%s%n",
                    srcBucket, srcKey, "dest-bucket-thumb", destKey);
        }
    }

    private void validateFile(String srcKey) {
        // Infer the image type.
        Matcher matcher = Pattern.compile(".*\\.([^\\.]*)").matcher(srcKey);
        if (!matcher.matches()) {
            throw new RuntimeException("Unable to infer image type for key " + srcKey);
        }

        String imageType = matcher.group(1);
        if (!(Config.JPG_TYPE.equals(imageType)) && !(Config.PNG_TYPE.equals(imageType)) && !(Config.JPEG_TYPE.equals(imageType))) {
            throw new RuntimeException("Skipping non-image " + srcKey);
        }
    }

    private String getBucketName(S3EventNotification.S3EventNotificationRecord eventRecord) {
        return eventRecord.getS3().getBucket().getName();
    }

    private String getKey(S3EventNotification.S3EventNotificationRecord eventRecord) throws UnsupportedEncodingException {
        String srcKey = eventRecord.getS3().getObject().getKey();
        // Object key may have spaces or unicode non-ASCII characters.
        return URLDecoder.decode(srcKey.replace('+', ' '), "UTF-8");
    }

    private String getContentType(String key){
        if(key.contains(Config.JPG_TYPE) || key.contains(Config.JPEG_TYPE)){
            return Config.JPG_MIME;
        }else{
            return Config.PNG_MIME;
        }
    }
}
