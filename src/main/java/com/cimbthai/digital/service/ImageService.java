package com.cimbthai.digital.service;

import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ImageService {

    private final S3Client s3Client;
    public ImageService() {
        this.s3Client = S3Client.builder().build();
    }

    // Download the image from S3 into a stream
    public InputStream getImageStream(String srcBucket, String srcKey) {
        GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                .bucket(srcBucket)
                .key(srcKey)
                .build();
        ResponseBytes<GetObjectResponse> objectAsBytes = s3Client.getObjectAsBytes(getObjectRequest);

        return objectAsBytes.asInputStream();
    }

    public void save(InputStream input, String key, int size, String contentType) {

        PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                .bucket("dest-bucket-thumb")
                .key(key)
                .contentType(contentType)
                .build();

        RequestBody requestBody = RequestBody.fromInputStream(input, size);
        s3Client.putObject(putObjectRequest, requestBody);
    }
}
