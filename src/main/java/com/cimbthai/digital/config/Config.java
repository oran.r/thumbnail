package com.cimbthai.digital.config;

@SuppressWarnings("WeakerAccess")
public class Config {
    private static Config config;
    public static final String JPG_TYPE = "jpg";
    public static final String JPEG_TYPE = "jpeg";
    public static final String JPG_MIME = "image/jpeg";
    public static final String PNG_TYPE = "png";
    public static final String PNG_MIME = "image/png";

    private int maxWidth;
    private int maxHeight;

    private Config(int maxWidth, int maxHeight) {
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
    }

    public static Config getInstance() {
        if (config == null) {
            config = new Config(200, 200);
        }
        return config;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    public int getMaxHeight() {
        return maxHeight;
    }

}
